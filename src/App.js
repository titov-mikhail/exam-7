import './App.css';
import {Component} from "react";
import DishComponent from "./Dish/DishComponent";
import {getFoodDishes} from "./Dish/DishesStore";
import {getDrinkDishes} from "./Dish/DishesStore";
import OrderDetailsComponent from "./Order/OrderDetailsComponent";
import RemoveButtonComponent from "./Order/RemoveButtonComponent";
import OrderDetail from "./Order/OrderDetail";
import EmptyOrderComponent from "./Order/EmptyOrderComponent";
import OrderSummaryComponent from "./Order/OrderSummaryComponent";

class App extends Component {
    state = {
        foodDishes: getFoodDishes(),
        drinkDishes: getDrinkDishes(),
        orderDetails: [],
        sum: 0
    }

    removeDishFromOrder = dishName => {
        const orderDetails = [...this.state.orderDetails];
        for (let i =0; i<orderDetails.length; i++) {
            if (orderDetails[i].name === dishName) {
                orderDetails.splice(i, 1);

                this.updateOrderDetails(orderDetails);
                return;
            }
        }
    }

    addDishToOrder = (dishName, price) => {
        const orderDetails = [...this.state.orderDetails];
        for (let i = 0; i< orderDetails.length; i++) {
            if (orderDetails[i].name === dishName) {
                orderDetails[i].qty++;
                this.updateOrderDetails(orderDetails);
                return;
            }
        }

        orderDetails.push(new OrderDetail(dishName, price));
        this.updateOrderDetails(orderDetails);
    }

    updateOrderDetails = (orderDetails) => {
        let sum = 0;
        for(let i = 0; i<orderDetails.length; i++) {
            sum += orderDetails[i].price * orderDetails[i].qty;
        }

        this.setState({
            orderDetails: orderDetails,
            sum: sum
        });
    }

  render() {
    return (
        <div className="App">
            <fieldset className='order-details'>
                <legend>Order details:</legend>
                <div className='bill-details'>
                    {this.state.orderDetails.length > 0
                        ?
                        <div>
                            <table>
                                <tbody>
                                    {this.state.orderDetails.map(d =>
                                        <OrderDetailsComponent
                                            dishName = {d.name}
                                            qty = {d.qty}
                                            price = {d.price}
                                            removeBtn = {
                                                <div className="remove-btn" onClick={() => this.removeDishFromOrder(d.name)}>
                                                    <RemoveButtonComponent/>
                                                </div>
                                            }
                                        />
                                    )}
                                    <OrderSummaryComponent
                                        sum = {this.state.sum}
                                    />
                                </tbody>
                            </table>
                        </div>
                        : <EmptyOrderComponent/>
                    }
                </div>
            </fieldset>
          <fieldset  className="dishes">
              <legend>Add items:</legend>
              <div className="food-dishes">
                    {
                        this.state.foodDishes.map((d) =>
                            <div onClick={ () => this.addDishToOrder(d.name, d.price) }>
                                <DishComponent
                                    name ={d.name}
                                    price ={d.price}
                                    icon ={d.icon}
                                />
                            </div>
                        )
                    }
                </div>
              <div className='drink-dishes'>
                  {
                      this.state.drinkDishes.map((d) =>
                          <div onClick={ () => this.addDishToOrder(d.name, d.price) }>
                              <DishComponent
                                  name ={d.name}
                                  price ={d.price}
                                  icon ={d.icon}
                              />
                          </div>
                      )
                  }
              </div>
          </fieldset>
        </div>
    );
  }
}

export default App;
