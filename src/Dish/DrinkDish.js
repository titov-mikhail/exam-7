import Dish from "./Dish";
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

class DrinkDish extends Dish {
    constructor(name, price) {
        super(name, price, faCoffee);
    }
}

export  default  DrinkDish;