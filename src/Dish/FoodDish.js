import Dish from "./Dish";
import {faUtensils} from "@fortawesome/free-solid-svg-icons";

class FoodDish extends Dish {
    constructor(name, price) {
        super(name, price, faUtensils);
    }
}

export  default  FoodDish;