class Dish {
    constructor(name, price, icon) {
        this.name = name;
        this.price = price;
        this.icon = icon;
    }
}

export default Dish;