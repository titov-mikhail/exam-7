import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './DishComponent.css';

const DishComponent = props => {
    return (
        <div className="Dish">
            <div className="icon">
                <FontAwesomeIcon icon={props.icon} />
            </div>
            <div>
                <h4>{props.name}</h4>
                <p>Price: {props.price} KGS</p>
            </div>
        </div>
    )
}

export default DishComponent;