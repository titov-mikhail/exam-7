import FoodDish from "./FoodDish";
import DrinkDish from "./DrinkDish";

export const getFoodDishes = () => {
        return [
            new FoodDish('Hamburger', 80),
            new FoodDish('Cheeseburger', 90),
            new FoodDish('Fries', 45)
        ];
    }

export const getDrinkDishes = () => {
    return  [
        new DrinkDish('Coffee', 80),
        new DrinkDish('Tea', 90),
        new DrinkDish('Cola', 45)
    ];
}