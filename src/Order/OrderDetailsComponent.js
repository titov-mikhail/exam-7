import React from "react";
import './RemoveButtonComponent.css';

const OrderDetailsComponent = props => {
    return (
        <tr>
            <td align='left' style={{paddingLeft: '10x'}}>{props.dishName}</td>
            <td>{props.qty} </td>
            <td>x</td>
            <td>{props.price} KGS</td>
            <td>{props.removeBtn}</td>
        </tr>
    )
}

export  default OrderDetailsComponent;