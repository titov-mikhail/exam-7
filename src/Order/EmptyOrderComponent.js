import React from "react";

const EmptyOrderComponent = () => {
    return (
        <div>
            <p>Order is empty!</p>
            <p>Please add some items!</p>
        </div>
    )
}

export default EmptyOrderComponent;