import React from "react";

const OrderSummaryComponent = props => {
    return (
        <tr>
            <td colSpan='3'><b>Total price </b></td>
            <td style={
                    {
                        borderTop: '2px solid black' ,
                        borderCollapse: 'collapse'
                    }
            }>
             <b>{props.sum} KGS</b></td>
        </tr>
    )
}

export default OrderSummaryComponent;