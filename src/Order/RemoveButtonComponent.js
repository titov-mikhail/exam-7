import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";
import './RemoveButtonComponent.css';

const RemoveButtonComponent = () => {
    return (
        <div className='remove-btn'>
            <FontAwesomeIcon icon={faTrashAlt} />
        </div>
    )
}
export default RemoveButtonComponent;