class OrderDetail {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.qty = 1;
    }
}
export default OrderDetail;